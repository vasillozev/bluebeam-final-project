using System.ComponentModel.DataAnnotations;

namespace backend.BodyDtos;
public class CreateSpaceDto
{
    [StringLength(50, MinimumLength = 2, ErrorMessage = "The name field must be between 2 and 50 characters.")]
    public string? name { get; set; }

    [StringLength(100, MinimumLength = 2, ErrorMessage = "The description field must be between 2 and 50 characters.")]
    public string? description { get; set; }

    public IFormFile? file { get; set; }
}
