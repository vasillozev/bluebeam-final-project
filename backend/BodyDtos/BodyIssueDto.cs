﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace backend.BodyDtos;
public class BodyIssueDto
{
    public int id { get; set; }

    [Required(ErrorMessage = "The name field is required")]
    [StringLength(50, MinimumLength = 2, ErrorMessage = "The title field must be between 2 and 50 characters.")]
    public required string title { get; set; }

    [Required(ErrorMessage = "The description field is required")]
    [StringLength(400, MinimumLength = 2, ErrorMessage = "The description field must be between 2 and 50 characters.")]
    public required string description { get; set; }

    [Range(1, int.MaxValue, ErrorMessage = "The assignee id must be a positive number.")]
    public int? assigneeId { get; set; }

    public IFormFile? file { get; set; }

    [Range(0, 2, ErrorMessage = "The status id must be 0, 1 or 2")]
    public int statusId { get; set; }
    [DataType(DataType.DateTime)]
    public required DateTime createdAt { get; set; }

    [DataType(DataType.DateTime)]
    public DateTime? resolvedAt { get; set; }

    [Required(ErrorMessage = "The coordX field is required")]
    public required double coordX { get; set; }

    [Required(ErrorMessage = "The coordY field is required")]
    public required double coordY { get; set; }

    [Range(1, int.MaxValue, ErrorMessage = "The space id must be a positive number.")]
    public required int spaceId { get; set; }

    [Range(1, int.MaxValue, ErrorMessage = "The facility id must be a positive number.")]
    public required int facilityId { get; set; }

}
