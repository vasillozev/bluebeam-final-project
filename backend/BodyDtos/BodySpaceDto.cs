﻿using System.ComponentModel.DataAnnotations;

namespace backend.BodyDtos;
public class BodySpaceDto
{
    public int id { get; set; }

    [Required(ErrorMessage = "The name field is required")]
    [StringLength(50, MinimumLength = 2, ErrorMessage = "The name field must be between 2 and 50 characters.")]
    public required string name { get; set; }

    [Required(ErrorMessage = "The description field is required")]
    [StringLength(100, MinimumLength = 2, ErrorMessage = "The description field must be between 2 and 50 characters.")]
    public required string description { get; set; }

    [Required(ErrorMessage = "The file field is required")]
    public required IFormFile? file { get; set; }

    [Range(1, int.MaxValue, ErrorMessage = "The facility id must be a positive number.")]
    public required int facilityId { get; set; }
}
