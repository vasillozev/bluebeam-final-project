﻿using AutoMapper;
using backend.Dtos;
using backend.BodyDtos;
using backend.Models;
using Microsoft.EntityFrameworkCore;
using backend.Services.ImageService;
using CloudinaryDotNet.Core;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Mvc;
using backend.Extensions;


namespace backend.Services.FacilityService
{
    public class FacilityService(SolarisContext context, IMapper mapper, IImageService imageService) : IFacilityService
    {
        private readonly SolarisContext _context = context;
        private readonly IMapper _mapper = mapper;

        public async Task<List<FacilityDto>> GetAllFacilitiesAsync()
        {
            var facilities = await _context.Facilities.ToListAsync();
            return facilities.Select(f => _mapper.Map<FacilityDto>(f)).ToList();
        }

        public async Task<List<FacilityWithCountDto>> GetAllFacilitiesWithIssueCountAsync()
        {
            var facilityDtos = await _context.Facilities
                .Select(f => new FacilityWithCountDto
                {
                    id = f.id,
                    name = f.name,
                    address = f.address,
                    photoUrl = f.photoUrl,
                    lat = f.lat,
                    lng = f.lng,
                    issuesCount = f.issues.Count
                })
                .ToListAsync();

            return facilityDtos;
        }

        public async Task<FacilityDto> GetFacilityByIdAsync(int id)
        {
            var facility = await _context.Facilities.FirstOrDefaultAsync(f => f.id == id);
            return _mapper.Map<FacilityDto>(facility);
        }

        public async Task<FacilityDto> AddFacility(BodyFacilityDto facilityFromForm)
        {
            var facilityWithPhotoUrl = await imageService.MapToFacilityWithPhotoUrl(facilityFromForm);

            await _context.Facilities.AddAsync(facilityWithPhotoUrl);
            var result = await _context.SaveChangesAsync();

            if (result < 1)
            {
                throw new Exception();
            }

            return mapper.Map<FacilityDto>(facilityWithPhotoUrl);
        }

        public async Task<FacilityDto> UpdateFacility(int id, CreateFacilityDto createBody)
        {
            var existingFacility = (await _context.Facilities.FirstOrDefaultAsync(f => f.id == id))!;

            if (createBody.name != null)
            {
                existingFacility.name = createBody.name;
            }

            if (createBody.address != null)
            {
                existingFacility.address = createBody.address;
            }

            if (createBody.lat != null)
            {
                existingFacility.lat = (double)createBody.lat;
            }

            if (createBody.lng != null)
            {
                existingFacility.lng = (double)createBody.lng;
            }

            if (createBody.file != null)
            {
                var uploadResult = await imageService.UploadAsync(createBody.file);

                if (uploadResult != null && uploadResult.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    existingFacility.photoUrl = uploadResult.SecureUrl.AbsoluteUri;
                }
                else
                {
                    throw new Exception($"Failed to upload file: {uploadResult?.Error?.Message}");
                }
            }

            await _context.SaveChangesAsync();

            return mapper.Map<FacilityDto>(existingFacility);
        }

        public async Task<bool> DeleteFacility(int id)
        {
            var existingFacility = _context.Facilities.FirstOrDefault(f => f.id == id);

            if (existingFacility == null)
            {
                throw new ArgumentException();
            }

            _context.Facilities.Remove(existingFacility);
            var result = await _context.SaveChangesAsync();

            if (result < 1)
            {
                throw new DbUpdateException();
            }

            return true;
        }
    }
}
