﻿using backend.BodyDtos;
using backend.Models;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using dotenv.net;
using System;

namespace backend.Services.ImageService;

public class ImageService : IImageService
{
    private readonly Cloudinary _cloudinary;
    private readonly long _maxSizeInMegaBytes = 5;
    private readonly long _oneMbInBytes = 1048576;
    private readonly long _maxSizeInBytes;
    private readonly string[] _allowedFormats = ["image/jpeg", "image/png"];

    public ImageService()
    {
        DotEnv.Load(options: new DotEnvOptions(probeForEnv: true));
        _cloudinary = new Cloudinary(Environment.GetEnvironmentVariable("CLOUDINARY_URL"));
        _cloudinary.Api.Secure = true;
        _maxSizeInBytes = _maxSizeInMegaBytes * _oneMbInBytes;
    }

    public async Task<ImageUploadResult?> UploadAsync(IFormFile file)
    {
        ValidateImage(file);

        using var stream = file.OpenReadStream();

        var uploadParams = new ImageUploadParams()
        {
            File = new FileDescription(file.FileName, stream)
        };

        var uploadResult = await _cloudinary.UploadAsync(uploadParams);

        return uploadResult;
    }

    public async Task<Facility> MapToFacilityWithPhotoUrl(BodyFacilityDto bodyFacilityDto)
    {
        var uploadResult = await UploadAsync(bodyFacilityDto.file);

        if (uploadResult != null && uploadResult.StatusCode == System.Net.HttpStatusCode.OK)
        {
            var url = uploadResult.SecureUrl.AbsoluteUri;

            var facility = new Facility
            {
                id = bodyFacilityDto.id,
                name = bodyFacilityDto.name,
                address = bodyFacilityDto.address,
                photoUrl = url,
                lat = bodyFacilityDto.lat,
                lng = bodyFacilityDto.lng
            };

            return facility;
        }
        else
        {
            throw new Exception($"Failed to upload file: {uploadResult?.Error?.Message}");
        }
    }

    public async Task<Space> MapToSpaceWithPhotoUrl(BodySpaceDto bodySpaceDto)
    {
        if (bodySpaceDto.file == null)
        {
            throw new ArgumentNullException(nameof(bodySpaceDto.file), "The file is missing");
        }

        var uploadResult = await UploadAsync(bodySpaceDto.file);

        if (uploadResult != null && uploadResult.StatusCode == System.Net.HttpStatusCode.OK)
        {
            var url = uploadResult.SecureUrl.AbsoluteUri;

            var space = new Space
            {
                id = bodySpaceDto.id,
                name = bodySpaceDto.name,
                description = bodySpaceDto.description,
                blueprintUrl = url,
                facilityId = bodySpaceDto.facilityId,
            };

            return space;
        }
        else
        {
            throw new Exception($"Failed to upload file: {uploadResult?.Error?.Message}");
        }
    }

    public async Task<Issue> MapToIssueWithPhotoUrl(BodyIssueDto bodyIssueDto)
    {
        if (bodyIssueDto.file == null)
        {
            throw new ArgumentNullException(nameof(bodyIssueDto.file), "The file is missing");
        }

        var uploadResult = await UploadAsync(bodyIssueDto.file);

        if (uploadResult != null && uploadResult.StatusCode == System.Net.HttpStatusCode.OK)
        {
            var url = uploadResult.SecureUrl.AbsoluteUri;

            var issue = new Issue
            {
                id = bodyIssueDto.id,
                title = bodyIssueDto.title,
                description = bodyIssueDto.description,
                assigneeId = bodyIssueDto.assigneeId,
                photoUrl = url,
                statusId = bodyIssueDto.statusId,
                createdAt = bodyIssueDto.createdAt,
                resolvedAt = bodyIssueDto.resolvedAt,
                coordX = bodyIssueDto.coordX,
                coordY = bodyIssueDto.coordY,
                spaceId = bodyIssueDto.spaceId,
                facilityId = bodyIssueDto.facilityId,
            };

            return issue;
        }
        else
        {
            throw new Exception($"Failed to upload file: {uploadResult?.Error?.Message}");
        }
    }

    public void ValidateImage(IFormFile file)
    {
        if (file == null || file.Length == 0)
        {
            throw new ArgumentNullException(nameof(file), "The file is missing!");
        }

        if (file.Length > _maxSizeInBytes)
        {
            throw new ArgumentException($"File size exceeds the {_maxSizeInMegaBytes}MB limit.");
        }

        if (!_allowedFormats.Contains(file.ContentType))
        {
            throw new ArgumentException("Invalid file type. Only JPEG and PNG files are allowed.");
        }
    }
}
