﻿using backend.BodyDtos;
using backend.Dtos;

namespace backend.Services.SpaceService
{
    public interface ISpaceService
    {
        Task<List<SpaceDto>> GetAllSpacesAsync();

        Task<SpaceDto> GetSpaceByIdAsync(int id);
        Task<List<SpaceDto>> GetSpacesByFacilityIdAsync(int id);

        Task<SpaceDto> AddSpace(BodySpaceDto spaceFromForm);

        Task<SpaceDto> UpdateSpace(int id, CreateSpaceDto createBody);

        Task<bool> DeleteSpace(int id);
    }
}
