﻿using AutoMapper;
using backend.BodyDtos;
using backend.Dtos;
using backend.Models;
using backend.Services.ImageService;
using Microsoft.EntityFrameworkCore;
using backend.Extensions;
using CloudinaryDotNet.Actions;

namespace backend.Services.SpaceService
{
    public class SpaceService(SolarisContext context, IMapper mapper, IImageService imageService) : ISpaceService
    {
        private readonly SolarisContext _context = context;

        public async Task<List<SpaceDto>> GetAllSpacesAsync()
        {
            var spaces = await _context.Spaces.ToListAsync();
            return spaces.Select(mapper.Map<SpaceDto>).ToList();
        }

        public async Task<SpaceDto> GetSpaceByIdAsync(int id)
        {
            var space = await _context.Spaces.FirstOrDefaultAsync(s => s.id == id);
            return mapper.Map<SpaceDto>(space);
        }

        public async Task<List<SpaceDto>> GetSpacesByFacilityIdAsync(int id)
        {
            var spaces = await _context.Spaces.Where(f => f.facilityId == id).ToListAsync();
            return spaces.Select(mapper.Map<SpaceDto>).ToList();
        }

        public async Task<SpaceDto> AddSpace(BodySpaceDto spaceFromForm)
        {
            var spaceWithPhotoUrl = await imageService.MapToSpaceWithPhotoUrl(spaceFromForm);

            await _context.Spaces.AddAsync(spaceWithPhotoUrl);
            var result = await _context.SaveChangesAsync();

            if (result < 1)
            {
                throw new Exception();
            }

            return mapper.Map<SpaceDto>(spaceWithPhotoUrl);
        }

        public async Task<SpaceDto> UpdateSpace(int id, CreateSpaceDto createBody)
        {
            var existingSpace = await _context.Spaces.FirstOrDefaultAsync(s => s.id == id);

            if (existingSpace == null)
            {
                throw new ArgumentException("Tried to update an space that does not exist");
            }

            if (createBody.name != null)
            {
                existingSpace.name = createBody.name;
            }

            if (createBody.description != null)
            {
                existingSpace.description = createBody.description;
            }

            if (createBody.file != null)
            {
                var uploadResult = await imageService.UploadAsync(createBody.file);

                if (uploadResult != null && uploadResult.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    existingSpace.blueprintUrl = uploadResult.SecureUrl.AbsoluteUri;
                }
                else
                {
                    throw new Exception($"Failed to upload file: {uploadResult?.Error?.Message}");
                }
            }

            await _context.SaveChangesAsync();

            return mapper.Map<SpaceDto>(existingSpace);
        }

        public async Task<bool> DeleteSpace(int id)
        {
            var existingSpace = _context.Spaces.FirstOrDefault(s => s.id == id);

            if (existingSpace == null)
            {
                throw new ArgumentException();
            }

            _context.Spaces.Remove(existingSpace);
            var result = await _context.SaveChangesAsync();

            if (result < 1)
            {
                throw new DbUpdateException();
            }

            return true;
        }
    }

}
