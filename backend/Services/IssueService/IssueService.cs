﻿using AutoMapper;
using backend.BodyDtos;
using backend.Dtos;
using backend.Models;
using backend.Services.ImageService;
using backend.Services.IssueService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend.Extensions;

namespace backend.Services.IssueService
{
    public class IssueService(SolarisContext context, IMapper mapper, IImageService imageService) : IIssueService
    {
        private readonly SolarisContext _context = context;
        private readonly IMapper _mapper = mapper;

        public async Task<List<IssueDto>> GetAllIssuesAsync([FromQuery] bool deep = false)
        {
            var issues = deep
            ? await _context.Issues.Include(i => i.facility).Include(i => i.space).ToListAsync()
            : await _context.Issues.ToListAsync();
            return issues.Select(i => _mapper.Map<IssueDto>(i)).ToList();
        }

        public async Task<IssueDto> GetIssueByIdAsync(int id)
        {
            var issue = await _context.Issues.FirstOrDefaultAsync(i => i.id == id);
            return _mapper.Map<IssueDto>(issue);
        }

        public async Task<List<IssueDto>> GetIssuesByFacilityIdAsync(int id)
        {
            var issues = await _context.Issues.Where(f => f.facilityId == id).ToListAsync();
            return issues.Select(i => _mapper.Map<IssueDto>(i)).ToList();
        }

        public async Task<List<IssueDto>> GetIssuesBySpaceIdAsync(int id)
        {
            var issues = await _context.Issues.Where(f => f.spaceId == id).ToListAsync();
            return issues.Select(i => _mapper.Map<IssueDto>(i)).ToList();
        }

        public async Task<IssueDto> AddIssue(BodyIssueDto issueFromForm)
        {
            Issue issueWithPhotoUrl;

            if (issueFromForm.file != null)
            {
                issueWithPhotoUrl = await imageService.MapToIssueWithPhotoUrl(issueFromForm);
            }
            else
            {
                issueWithPhotoUrl = new Issue
                {
                    id = issueFromForm.id,
                    title = issueFromForm.title,
                    description = issueFromForm.description,
                    assigneeId = issueFromForm.assigneeId,
                    photoUrl = null,
                    statusId = issueFromForm.statusId,
                    createdAt = issueFromForm.createdAt,
                    resolvedAt = issueFromForm.resolvedAt,
                    coordX = issueFromForm.coordX,
                    coordY = issueFromForm.coordY,
                    spaceId = issueFromForm.spaceId,
                    facilityId = issueFromForm.facilityId,
                };
            }

            await _context.Issues.AddAsync(issueWithPhotoUrl);
            var result = await _context.SaveChangesAsync();

            if (result < 1)
            {
                throw new Exception();
            }

            return mapper.Map<IssueDto>(issueWithPhotoUrl);
        }

        public async Task<IssueDto> UpdateIssue(int id, CreateIssueDto createBody)
        {
            var existingIssue = await _context.Issues.FirstOrDefaultAsync(i => i.id == id);

            if (existingIssue == null)
            {
                throw new ArgumentException("Tried to update an issue that does not exist");
            }

            // if there is no file to upload(we cannot check file for equality) but name and address match then the request is gonna be pointless

            if (createBody.title != null)
            {
                existingIssue.title = createBody.title;
            }

            if (createBody.description != null)
            {
                existingIssue.description = createBody.description;
            }

            if (createBody.coordX != null)
            {
                existingIssue.coordX = (double)createBody.coordX;
            }

            if (createBody.coordY != null)
            {
                existingIssue.coordY = (double)createBody.coordY;
            }

            if (createBody.statusId != null)
            {
                existingIssue.statusId = (int)createBody.statusId;
            }

            if (createBody.assigneeId != null)
            {
                existingIssue.assigneeId = (int)createBody.assigneeId;
            }
            else
            {
                existingIssue.assigneeId = null;
            }

            existingIssue.resolvedAt = createBody.resolvedAt;

            if (createBody.file != null)
            {
                var uploadResult = await imageService.UploadAsync(createBody.file);

                if (uploadResult != null && uploadResult.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    existingIssue.photoUrl = uploadResult.SecureUrl.AbsoluteUri;
                }
                else
                {
                    throw new Exception($"Failed to upload file: {uploadResult?.Error?.Message}");
                }
            }

            await _context.SaveChangesAsync();

            return mapper.Map<IssueDto>(existingIssue);
        }

        public async Task<bool> DeleteIssue(int id)
        {
            var existingIssue = _context.Issues.FirstOrDefault(i => i.id == id);

            if (existingIssue == null)
            {
                throw new ArgumentException();
            }

            _context.Issues.Remove(existingIssue);
            var result = await _context.SaveChangesAsync();

            if (result < 1)
            {
                throw new DbUpdateException();
            }

            return true;
        }
    }
}
