﻿using backend.BodyDtos;
using backend.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace backend.Services.IssueService
{
    public interface IIssueService
    {
        Task<List<IssueDto>> GetAllIssuesAsync(bool deep = false);

        Task<IssueDto> GetIssueByIdAsync(int id);
        Task<List<IssueDto>> GetIssuesByFacilityIdAsync(int id);

        Task<List<IssueDto>> GetIssuesBySpaceIdAsync(int id);

        Task<IssueDto> AddIssue(BodyIssueDto issueFromForm);

        Task<IssueDto> UpdateIssue(int id, CreateIssueDto createBody);

        Task<bool> DeleteIssue(int id);
    }
}
