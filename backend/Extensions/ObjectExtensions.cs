namespace backend.Extensions;

public static class ObjectExtensions
{
    /// <summary>
    /// Checks if all properties of an object are null
    /// </summary>
    /// <param name="obj"></param>
    /// <returns>true if all properties are null</returns>
    public static bool AllPropertiesAreNull(this object obj)
    {
        if (obj == null) return true;

        foreach (var property in obj.GetType().GetProperties())
        {
            if (property.GetValue(obj) != null)
            {
                return false;
            }
        }

        return true;
    }

    public static bool AllPropertiesMatch(this object obj, object other)
    {
        if (obj == null || other == null) return false;

        foreach (var property in obj.GetType().GetProperties())
        {
            if (property.GetValue(obj)?.ToString() != property.GetValue(other)?.ToString())
            {
                return false;
            }
        }

        return true;
    }
}
