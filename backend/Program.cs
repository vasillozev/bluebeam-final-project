using backend.Services.ImageService;

using backend.Models;
using backend.Profiles;
using dotenv.net;
using backend.Services;
using backend.Services.FacilityService;
using backend.Services.SpaceService;
using backend.Services.IssueService;

DotEnv.Load(options: new DotEnvOptions(probeForEnv: true));

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IImageService, ImageService>();
builder.Services.AddControllers();

builder.Services.AddDbContext<SolarisContext>();

builder.Services.AddCors(
  options =>
  {
      options.AddDefaultPolicy(
        builder => builder
          .WithOrigins("http://localhost:3000", "http://localhost:5173", "http://localhost:5174")
          .WithMethods("GET", "POST", "PUT", "DELETE", "OPTIONS", "HEAD")
          .AllowAnyHeader()
      );
  }
);

builder.Services.AddAutoMapper(typeof(FacilityProfile));
builder.Services.AddAutoMapper(typeof(IssueProfile));
builder.Services.AddAutoMapper(typeof(FacilityProfile));

builder.Services.AddScoped<IFacilityService, FacilityService>();
builder.Services.AddScoped<ISpaceService, SpaceService>();
builder.Services.AddScoped<IIssueService, IssueService>();

var app = builder.Build();
app.UsePathBase("/api/v1/");

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors();

app.UseHttpsRedirection();

app.MapControllers();

app.Run();
