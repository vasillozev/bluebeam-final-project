using System.ComponentModel.DataAnnotations;

namespace backend.Dtos;
public class IssueDto
{
    public int id { get; set; }

    [Required(ErrorMessage = "The name field is required")]
    public required string title { get; set; }

    [Required(ErrorMessage = "The description field is required")]
    public required string description { get; set; }

    public int? assigneeId { get; set; }

    public string? photoUrl { get; set; }

    public int statusId { get; set; }
    public required DateTime createdAt { get; set; }
    public DateTime? resolvedAt { get; set; }

    [Required(ErrorMessage = "The coordX field is required")]
    public required double coordX { get; set; }

    [Required(ErrorMessage = "The coordY field is required")]
    public required double coordY { get; set; }

    public required int spaceId { get; set; }

    public SpaceDto? space { get; set; }

    public required int facilityId { get; set; }

    public FacilityDto? facility { get; set; }

}
