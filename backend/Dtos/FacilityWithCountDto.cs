using backend.Models;
using System.ComponentModel.DataAnnotations;

namespace backend.Dtos;
public class FacilityWithCountDto
{
    public int id { get; set; }

    [Required(ErrorMessage = "The name field is required")]
    public required string name { get; set; }

    [Required(ErrorMessage = "The address field is required")]
    public required string address { get; set; }

    public required string photoUrl { get; set; }

    [Range(-90, 90, ErrorMessage = "The latitude must be between -90 and 90.")]
    public double lat { get; set; }

    [Range(-180, 180, ErrorMessage = "The longitude must be between -180 and 180.")]
    public double lng { get; set; }

    public int? issuesCount { get; set; }
}
