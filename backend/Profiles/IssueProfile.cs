﻿using backend.Models;
using AutoMapper;
using backend.Dtos;

namespace backend.Profiles;

public class IssueProfile : Profile
{
  public IssueProfile()
  {
    CreateMap<Issue, IssueDto>().ReverseMap();
  }
}

