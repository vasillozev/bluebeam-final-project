using AutoMapper;
using backend.BodyDtos;
using backend.Dtos;
using backend.Models;
using backend.Services.ImageService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend.Extensions;
using CloudinaryDotNet.Core;
using backend.Services.IssueService;
using backend.Services.SpaceService;

// /api/v1/issue
[ApiController]
[Route("issue")]
public class IssuesAPIController(IIssueService issueService, IImageService imageService, IMapper mapper) : ControllerBase
{
    // GET /api/v1/issue/all 
    [HttpGet("all")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> All([FromQuery] bool deep = false)
    {
        var issues = await issueService.GetAllIssuesAsync(deep);

        if (issues == null)
        {
            return StatusCode(500, new { message = "Internal server error while getting all issues!" });
        }

        if (issues?.Count == 0)
        {
            return NoContent();
        }

        return Ok(issues);
    }

    // GET /api/v1/issue/{id}
    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetById(int id)
    {
        if (id <= 0)
        {
            return StatusCode(500, new { message = $"Internal server error while getting issue with id:{id}!" });
        }

        var issue = await issueService.GetIssueByIdAsync(id);

        if (issue == null)
        {
            return StatusCode(500, new { message = $"Internal server error while getting issue with id:{id}!" });
        }

        return Ok(issue);
    }

    // GET /api/v1/issue/byspace/{spaceId}
    [HttpGet("byspace/{spaceId}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetBySpaceId(int spaceId)
    {
        if (spaceId <= 0)
        {
            return StatusCode(500, new { message = $"Internal server error while getting issue with id:{spaceId}!" });
        }

        var issues = await issueService.GetIssuesBySpaceIdAsync(spaceId);

        if (issues == null)
        {
            return StatusCode(500, new { message = $"Internal server error while getting issue with id:{spaceId}!" });
        }

        return Ok(issues);
    }

    // GET /api/v1/issue/byfacility/{facilityId}
    [HttpGet("byfacility/{facilityId}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetByFacilityId(int facilityId)
    {
        if (facilityId <= 0)
        {
            return StatusCode(500, new { message = $"Internal server error while getting issue with id:{facilityId}!" });
        }

        var issues = await issueService.GetIssuesByFacilityIdAsync(facilityId);

        if (issues == null)
        {
            return StatusCode(500, new { message = $"Internal server error while getting issue with id:{facilityId}!" });
        }

        return Ok(issues);
    }

    // POST /api/v1/issue
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Create([FromForm] BodyIssueDto issueFromForm)
    {
        if (issueFromForm == null)
        {
            return BadRequest(new { message = "Request body is empty." });
        }

        try
        {
            var existingIssue = await issueService.GetIssueByIdAsync(issueFromForm.id);

            if (existingIssue != null)
            {
                return Conflict(new { message = $"Issue with id:{issueFromForm.id} already exists!" });
            }

            try
            {
                var createdIssue = await issueService.AddIssue(issueFromForm);
                return CreatedAtAction("Create", createdIssue);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        catch (Exception)
        {
            return StatusCode(500, new { message = $"Internal server error while creating issue with id:{issueFromForm.id}!" });
        }
    }

    // PUT /api/v1/issue/{id}
    [HttpPut("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Update([FromRoute] int id, [FromForm] CreateIssueDto createBody)
    {
        if (id <= 0 || createBody == null)
        {
            return BadRequest(new { message = "Request body is invalid or empty." });
        }

        /*if (createBody.AllPropertiesAreNull())
        {
            return BadRequest(new { message = "Nothing to update, specify at least one property" });
        }*/

        try
        {
            var existingIssue = await issueService.GetIssueByIdAsync(id);
            if (existingIssue == null)
            {
                return NotFound(new { message = $"Issue with id: {id} is not found!" });
            }

            return Ok(await issueService.UpdateIssue(id, createBody));
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return StatusCode(500, new { message = $"Internal server error while updating issue with id: {id}!" });
        }
    }

    // DELETE /api/v1/issue/{id}
    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Delete(int id)
    {
        try
        {
            var issue = await issueService.GetIssueByIdAsync(id);

            if (issue == null)
            {
                return NotFound(new { message = $"Issue with this id:{id} is not found!" });
            }

            try
            {
                await issueService.DeleteIssue(id);
            }
            catch (ArgumentException)
            {
                return NotFound(new { message = $"Issue with this id:{id} is not found!" });
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }

            return Ok($"Successfully deleted issue with id:{id}!");
        }
        catch (Exception)
        {
            return StatusCode(500, new { message = "Something went wrong while deleting issue!" });
        }
    }
}
