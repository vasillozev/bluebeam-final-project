using backend.Dtos;
using backend.BodyDtos;
using backend.Models;
using backend.Services.ImageService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using backend.Extensions;
using backend.Services.FacilityService;
using CloudinaryDotNet.Core;


// /api/v1/facility
[ApiController]
[Route("facility")]
public class FacilitiesAPIController(IFacilityService facilityService) : ControllerBase
{
    // GET /api/v1/facility/all
    [HttpGet("all")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> All()
    {
        var facilities = await facilityService.GetAllFacilitiesAsync();

        if (facilities == null)
        {
            return StatusCode(500, new { message = "Internal server error while getting all facilities!" });
        }

        if (facilities?.Count == 0)
        {
            return NoContent();
        }

        return Ok(facilities);
    }

    // GET /api/v1/facility/all
    [HttpGet("all/count")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> AllWithIssueCount()
    {
        var facilities = await facilityService.GetAllFacilitiesWithIssueCountAsync();

        if (facilities == null)
        {
            return StatusCode(500, new { message = "Internal server error while getting all facilities!" });
        }

        if (facilities?.Count == 0)
        {
            return NoContent();
        }

        return Ok(facilities);
    }

    // GET /api/v1/facility/{id}
    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetById(int id)
    {
        if (id <= 0)
        {
            return StatusCode(500, new { message = $"Internal server error while getting facility with id:{id}!" });
        }

        var facility = await facilityService.GetFacilityByIdAsync(id);

        if (facility == null)
        {
            return StatusCode(500, new { message = $"Internal server error while getting facility with id:{id}!" });
        }

        return Ok(facility);
    }

    // POST /api/v1/facility
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Create([FromForm] BodyFacilityDto facilityFromForm)
    {
        if (facilityFromForm == null)
        {
            return BadRequest(new { message = "Request body is empty." });
        }

        try
        {
            var existingFacility = await facilityService.GetFacilityByIdAsync(facilityFromForm.id);

            if (existingFacility != null)
            {
                return Conflict(new { message = $"Facility with id:{facilityFromForm.id} already exists!" });
            }

            try
            {
                var createdFacility = await facilityService.AddFacility(facilityFromForm);
                return CreatedAtAction("Create", createdFacility);
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }
        catch (Exception)
        {
            return StatusCode(500, new { message = $"Internal server error while creating facility with id:{facilityFromForm.id}!" });
        }
    }

    // PUT /api/v1/facility/{id}
    [HttpPut("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Update([FromRoute] int id, [FromForm] CreateFacilityDto createBody)
    {
        if (id <= 0 || createBody == null)
        {
            return BadRequest(new { message = "Request body is invalid or empty." });
        }

        if (createBody.AllPropertiesAreNull())
        {
            return BadRequest(new { message = "Nothing to update, specify at least one property" });
        }

        try
        {
            var existingFacility = await facilityService.GetFacilityByIdAsync(id);
            if (existingFacility == null)
            {
                return NotFound(new { message = $"Facility with id: {id} is not found!" });
            }

            return Ok(await facilityService.UpdateFacility(id, createBody));
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return StatusCode(500, new { message = $"Internal server error while updating facility with id: {id}!" });
        }
    }

    // DELETE /api/v1/facility/{id}
    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Delete(int id)
    {
        try
        {
            var facility = await facilityService.GetFacilityByIdAsync(id);

            if (facility == null)
            {
                return NotFound(new { message = $"Facility with this id:{id} is not found!" });
            }

            try
            {
                await facilityService.DeleteFacility(id);
            }
            catch (ArgumentException)
            {
                return NotFound(new { message = $"Facility with this id:{id} is not found!" });
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }

            return Ok($"Successfully deleted facility with id:{id}!");
        }
        catch (Exception)
        {
            return StatusCode(500, new { message = "Something went wrong while deleting facility!" });
        }
    }
}
