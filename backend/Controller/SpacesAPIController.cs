using AutoMapper;
using backend.Dtos;
using backend.Models;
using backend.Services.ImageService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using backend.BodyDtos;
using backend.Extensions;
using backend.Services.SpaceService;
using backend.Services.FacilityService;

namespace backend.Controller
{
    // /api/v1/space
    [ApiController]
    [Route("space")]
    public class SpacesAPIController(ISpaceService spaceService, IImageService imageService, IMapper mapper) : ControllerBase
    {
        // GET /api/v1/space/all
        [HttpGet("all")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> All()
        {
            var spaces = await spaceService.GetAllSpacesAsync();

            if (spaces == null)
            {
                return StatusCode(500, new { message = "Internal server error while getting all spaces!" });
            }

            if (spaces?.Count == 0)
            {
                return NoContent();
            }

            return Ok(spaces);
        }

        // GET /api/v1/space/{id}
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetById(int id)
        {
            if (id <= 0)
            {
                return BadRequest(new { message = $"Not valid id!" });
            }

            var space = await spaceService.GetSpaceByIdAsync(id);

            if (space == null)
            {
                return StatusCode(500, new { message = $"Internal server error while getting space with id:{id}!" });
            }

            return Ok(space);
        }

        // GET /api/v1/space/byfacility/{facilityId}
        [HttpGet("byfacility/{facilityId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetByFacilityId(int facilityId)
        {
            if (facilityId <= 0)
            {
                return BadRequest(new { message = $"Not valid id!" });
            }

            var spaces = await spaceService.GetSpacesByFacilityIdAsync(facilityId);

            if (spaces == null)
            {
                return StatusCode(500, new { message = $"Internal server error while getting spaces with facility id:{facilityId}!" });
            }

            return Ok(spaces);
        }

        // POST /api/v1/space
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Create([FromForm] BodySpaceDto spaceFromForm)
        {
            if (spaceFromForm == null)
            {
                return BadRequest(new { message = "Request body is empty." });
            }

            try
            {
                var existingSpace = await spaceService.GetSpaceByIdAsync(spaceFromForm.id);

                if (existingSpace != null)
                {
                    return Conflict(new { message = $"Space with id:{spaceFromForm.id} already exists!" });
                }

                try
                {
                    var createdSpace = await spaceService.AddSpace(spaceFromForm);
                    return CreatedAtAction("Create", createdSpace);
                }
                catch (Exception)
                {
                    return BadRequest();
                }
            }
            catch (Exception)
            {
                return StatusCode(500, new { message = $"Internal server error while creating space with id:{spaceFromForm.id}!" });
            }
        }

        // PUT /api/v1/space/{id}
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Update([FromRoute] int id, [FromForm] CreateSpaceDto createBody)
        {
            if (id <= 0 || createBody == null)
            {
                return BadRequest(new { message = "Request body is invalid or empty." });
            }

            if (createBody.AllPropertiesAreNull())
            {
                return BadRequest(new { message = "Nothing to update, specify at least one property" });
            }

            try
            {
                var existingSpace = await spaceService.GetSpaceByIdAsync(id);
                if (existingSpace == null)
                {
                    return NotFound(new { message = $"Space with id: {id} is not found!" });
                }

                return Ok(await spaceService.UpdateSpace(id, createBody));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return StatusCode(500, new { message = $"Internal server error while updating space with id: {id}!" });
            }
        }

        // DELETE /api/v1/space/{id}
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var space = await spaceService.GetSpaceByIdAsync(id);

                if (space == null)
                {
                    return NotFound(new { message = $"Space with this id:{id} is not found!" });
                }

                try
                {
                    await spaceService.DeleteSpace(id);
                }
                catch (ArgumentException)
                {
                    return NotFound(new { message = $"Space with this id:{id} is not found!" });
                }
                catch (DbUpdateException)
                {
                    return BadRequest();
                }

                return Ok($"Successfully deleted space with id:{id}!");
            }
            catch (Exception)
            {
                return StatusCode(500, new { message = "Something went wrong while deleting space!" });
            }
        }
    }
}
