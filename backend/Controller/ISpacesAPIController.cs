﻿using backend.Dtos;
using backend.Models;

namespace backend.Controller
{
    public interface ISpacesAPIController
    {
        List<Space> All();
        SpaceDto Create(SpaceDto space);
        void Delete(int id);
        SpaceDto GetById(int id);
        SpaceDto Update(int id, SpaceDto space);
    }
}