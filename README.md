# Facility Management Software

This software is designed for facility managers and workers to streamline communication and management tasks. The application allows users to choose between roles, either as a Facility Manager (FM) or as a worker.

## Key Features

### Facility Manager (FM) Role:

- **World Map Overview:** View facilities managed on a world map and create new facilities.
- **Facilities Management:**
  - **Facilities List:** View a list of all facilities and perform operations such as adding, updating, or deleting facilities.
- **Spaces and Plans Management:**
  - **Spaces Overview:** Navigate through spaces within each facility.
  - **Plan View:** Access detailed plans of spaces, enabling the creation of issues directly on the plans.
- **Dashboard:** Monitor all issues across facilities and their respective statuses.

### Worker Role:

- **Dashboard:** View a dashboard displaying issues assigned to the worker.

This software aims to enhance operational efficiency and communication between facility managers and workers, making facility management more streamlined and effective.

## Table of Contents

- [Installation](#installation)
  - [Backend Installation](#backend-installation)
  - [Frontend Installation](#frontend-installation)
- [Contributing](#contributing)
- [Contact](#contact)

## Installation

To run this project locally, you'll need to set up both the backend API and the frontend application.

### Backend Installation

The backend is a .NET web API project.

1. Clone the repository:

   ```bash
   git clone git@gitlabcom:davidtemelkovbluebeam-final-project.git
   ```

2. Navigate to the backend folder:

   ```bash
   cd backend
   ```

3. Restore NuGet packages and build the project:

   ```bash
   dotnet restore
   dotnet build
   ```

4. Run the backend server:

   ```bash
   dotnet run
   ```

The backend server will start at http://localhost:8081.

### Frontend Installation

The frontend is a TypeScript and React application with Tailwind CSS.

1. Clone the repository:

   ```bash
   git clone git@gitlabcom:davidtemelkovbluebeam-final-project.git
   ```

2. Navigate to the frontend folder:

   ```bash
   cd frontend
   ```

3. Install dependencies:

   ```bash
   npm install
   ```

4. Start the frontend development server:

   ```bash
   npm run dev
   ```

5. Open your browser and navigate to http://localhost:5173 to view the frontend.

## Contributing

1. Fork the repository
2. Create a new branch (`git checkout -b feat/your-feature`)
3. Commit your changes (`git commit -m 'Add some feature'`)
4. Push to the branch (`git push origin feat/your-feature`)
5. Open a merge request

## Contact

Rado Kolev - https://www.linkedin.com/in/rakolev/

Dimo Dimov - https://www.linkedin.com/in/dimorvdimov/

Izabel Tasheva - https://www.linkedin.com/in/izabel-tasheva-3bb5352b0/

Vasil Lozev - https://www.linkedin.com/in/vasil-lozev-7a4932227/

Project Link: [https://gitlab.com/davidtemelkov/bluebeam-final-project](https://gitlab.com/davidtemelkov/bluebeam-final-project)
