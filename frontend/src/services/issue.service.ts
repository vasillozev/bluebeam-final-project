/* eslint-disable @typescript-eslint/no-unused-vars */
// TODO: remove eslint-disable

import { Status } from "@/utils/enums/status";
import { FacilityResponse } from "./facility.service";
import { SpaceResponse } from "./space.service";

type IssueBase = {
  id: number;
  title: string;
  assigneeId: number | null;
  description: string;
  statusId: Status.TODO | Status.IN_PROGRESS | Status.DONE;
  createdAt: string;
  resolvedAt: string | null;
  spaceId: string;
  space: SpaceResponse | null;
  facilityId: string;
  facility: FacilityResponse | null;
};

export type IssueLocation = {
  coordY: number;
  coordX: number;
};

type Issue = IssueBase & IssueLocation;

export type IssueBody = Issue & {
  file: File | null;
};

export type IssueResponse = Issue & {
  photoUrl: string | null;
};

export type UpdateIssueData = { id: number } & Partial<{
  title: string;
  description: string;
  coordX: number;
  coordY: number;
  statusId: number;
  resolvedAt: string | null;
  assigneeId: number;
  file: File;
}>;

export interface IIssueService {
  getAll(deep?: boolean): Promise<IssueResponse[]>;
  getById(id: number | string): Promise<IssueResponse | null>;
  getBySpace(spaceId: number | string): Promise<IssueResponse[]>;
  getByFacility(facilityId: string): Promise<IssueResponse[]>;

  deleteById(id: string): void;
  update(updated: UpdateIssueData): Promise<IssueResponse>;
  create(issue: Omit<IssueBody, "id">): Promise<IssueResponse>;
}

const backendUrl =
  typeof import.meta.env.VITE_BACKEND_URL === "string"
    ? import.meta.env.VITE_BACKEND_URL
    : "http://localhost:8081";
const endpoint = backendUrl + "/api/v1/issue";

const issueService: IIssueService = {
  getAll: async function (deep = false) {
    const response = await fetch(`${endpoint}/all?deep=${deep}`);

    if (!response.ok) {
      throw new Error("Failed to fetch facilities");
    }

    return response.json();
  },
  getById: async function (id) {
    const response = await fetch(`${endpoint}/${id}`);

    if (!response.ok) {
      throw new Error("Failed to fetch data");
    }

    return await response.json();
  },
  getBySpace: async function (spaceId) {
    const response = await fetch(`${endpoint}/byspace/${spaceId}`);

    if (!response.ok) {
      throw new Error("Failed to fetch issues");
    }

    const issues: IssueResponse[] = await response.json();

    return issues;
  },

  getByFacility: async function (facilityId) {
    const response = await fetch(`${endpoint}/byfacility/${facilityId}`);

    if (!response.ok) {
      throw new Error("Failed to fetch facilities");
    }

    const issues: IssueResponse[] = await response.json();

    return issues;
  },
  deleteById: async function (id) {
    const response = await fetch(`${endpoint}/${id}`, {
      method: "DELETE",
    });

    if (!response.ok) {
      throw new Error("Failed to delete");
    }
  },
  update: async function ({ id, file, ...rest }) {
    const formData = new FormData();
    Object.entries(rest).forEach(([property, value]) => {
      if (value !== null && value !== undefined) {
        formData.set(property, value.toString());
      }
    });

    if (file) {
      formData.set("file", file);
    }

    const response = await fetch(`${endpoint}/${id}`, {
      method: "PUT",
      body: formData,
    });

    if (!response.ok) {
      throw new Error("Failed to update issue");
    }

    return response.json();
  },
  create: async function (issue: Omit<IssueBody, "id">) {
    const formData = new FormData();

    formData.append("title", issue.title);
    formData.append("description", issue.description);

    if (issue.assigneeId !== null && issue.assigneeId !== undefined) {
      formData.append("assigneeId", issue.assigneeId.toString());
    }

    formData.append("statusId", issue.statusId.toString());
    formData.append("createdAt", issue.createdAt.toString());
    formData.append(
      "resolvedAt",
      issue.resolvedAt ? issue.resolvedAt.toString() : "",
    );

    formData.append("spaceId", issue.spaceId.toString());
    formData.append("facilityId", issue.facilityId.toString());
    formData.append("coordX", issue.coordX.toString());
    formData.append("coordY", issue.coordY.toString());

    if (issue.file) {
      formData.append("file", issue.file);
    }

    const response = await fetch(endpoint, {
      method: "POST",
      body: formData,
      // headers: {
      //   "Content-Type": "multipart/form-data",
      // },
    });

    if (!response.ok) {
      throw new Error("Failed to create issue");
    }

    const createdIssue = await response.json();

    return createdIssue;
  },
};

export const {
  getAll,
  getById,
  getBySpace,
  getByFacility,
  deleteById,
  update,
  create,
} = issueService;
