/* eslint-disable @typescript-eslint/no-unused-vars */
// TODO: remove eslint-disable

export type SpaceBase = {
  id: string;
  name: string;
  description: string;
  facilityId: string;
};
export type SpaceBody = SpaceBase & {
  file: File;
};

export type SpaceResponse = SpaceBase & {
  blueprintUrl: string;
};

export type UpdateFacilityData = { id: string } & Partial<{
  name: string;
  description: string;
  file: File;
}>;

const backendUrl = typeof import.meta.env.VITE_BACKEND_URL === "string" ? import.meta.env.VITE_BACKEND_URL : "http://localhost:8081";
const endpoint = backendUrl + "/api/v1/space";

export interface ISpaceService {
  getAll(): Promise<SpaceResponse[]>;
  getById(id: number | string): Promise<SpaceResponse>;
  getByFacility(facilityId: number | string): Promise<SpaceResponse[]>;
  deleteById(id: number | string): Promise<void>;
  update(updated: UpdateFacilityData): Promise<SpaceResponse>;
  create(space: Omit<SpaceBody, "id">): Promise<SpaceResponse>;
}

const spaceService: ISpaceService = {
  getAll: async function () {
    throw new Error("Function not implemented.");
  },
  getById: async function (id) {
    const response = await fetch(`${endpoint}/${id}`);

    if (!response.ok) {
      throw new Error("Failed to fetch space");
    }

    const space: SpaceResponse = await response.json();

    return space;
  },
  getByFacility: async function (facilityId) {
    const response = await fetch(`${endpoint}/byfacility/${facilityId}`);

    if (!response.ok) {
      throw new Error("Failed to fetch spaces");
    }

    const facilities = await response.json();

    return facilities;
  },
  deleteById: async function (id) {
    const response = await fetch(`${endpoint}/${id}`, {
      method: "DELETE",
    });

    if (!response.ok) {
      throw new Error("Failed to delete space");
    }
  },
  update: async function ({ id, file, description, name }) {
    const formData = new FormData();
    if (name) formData.append("name", name);
    if (description) formData.append("description", description);
    if (file) formData.append("file", file);

    const response = await fetch(`${endpoint}/${id}`, {
      method: "PUT",
      body: formData,
    });

    if (!response.ok) {
      throw new Error("Failed to update space");
    }

    return response.json();
  },
  create: async function (space) {
    const formData = new FormData();
    formData.append("name", space.name);
    formData.append("description", space.description);
    formData.append("file", space.file);
    formData.append("facilityId", space.facilityId.toString());

    const response = await fetch(endpoint, {
      method: "POST",
      body: formData,
    });

    if (!response.ok) {
      throw new Error("Failed to create space");
    }

    const createdSpace = await response.json();

    return createdSpace;
  },
};

export const { getAll, getById, getByFacility, deleteById, update, create } =
  spaceService;
