import { useContext } from "react";

import { Link } from "react-router-dom";

import { Button } from "@/components/ui/button";
import { BriefcaseBusiness, Drill } from "lucide-react";

import { UserContext } from "@/components/Context/UserContext";
import { Role } from "@/utils/enums/role";

function HomeButton({
  link,
  children,
  title,
  handleClick,
}: {
  link: string;
  title: string;
  children: React.ReactNode;
  handleClick: () => void;
}) {
  return (
    <Link to={link}>
      <Button
        onClick={handleClick}
        variant="outline"
        className="h-6 w-full flex-row gap-8 rounded-xl border-2 border-primary/50 bg-primary/25 py-10 transition duration-150 hover:scale-105 hover:bg-primary hover:text-white md:w-[300px]"
      >
        {children}
        <span className="text-2xl font-semibold">{title}</span>
      </Button>
    </Link>
  );
}

export const HomePage: React.FC = () => {
  const { setUser } = useContext(UserContext)!;

  const handleManagerClick = () => {
    setUser({ name: "Benjamin Carter", role: Role.MANAGER, id: 11 });
  };

  const handleWorkerClick = () => {
    setUser({ name: "John Doe", role: Role.WORKER, id: 1 });
  };

  return (
    <div className="container flex flex-col items-center justify-center gap-8 md:flex-row md:justify-evenly">
      <div className="w-full gap-8 md:justify-center lg:max-w-[55%] lg:flex-col">
        <h2 className="line-height: 1,75 mb-4 text-center text-3xl font-black sm:text-4xl md:mt-0 md:text-left lg:mt-6 lg:text-5xl">
          Boost your efficiency with Solaris, making <br />
          <span className="text-orange-400"> facility management </span>
          <br />
          easy and effective.
        </h2>
        <p className="text-center text-xl font-semibold md:text-left">
          Solaris revolutionizes facility management by seamlessly connecting
          teams with reliable handy workers, offering an easily accessible,
          single source of truth for all your needs.
        </p>
        <div className="mt-8 flex flex-col gap-8 md:flex-row ">
          <HomeButton
            handleClick={handleManagerClick}
            link="/map"
            title="Manager"
          >
            <BriefcaseBusiness size={45} />
          </HomeButton>

          <HomeButton
            handleClick={handleWorkerClick}
            link="/issues"
            title="Worker"
          >
            <Drill size={45} />
          </HomeButton>
        </div>
      </div>
      <div className="ml-4 md:flex-row md:justify-evenly">
        <img
          className="h-50 hidden w-[600px] min-w-[450px] rounded-xl opacity-90 lg:block"
          src={"/homePagePlan.webp"}
          alt="planPhoto"
        />
      </div>
    </div>
  );
};
