import React from "react";
import { Button } from "@/components/ui/button";
import { Link } from "react-router-dom";

export const NotFound: React.FC = () => {
  return (
    <div className="flex flex-col items-center justify-center gap-9">
      <h1 className="text-2xl font-semibold md:text-4xl">404 Page Not Found</h1>
      <p>
        We can't seem to find this page. In the meantime, can we help you with
        something else?
      </p>
      <Link to="/">
        <Button
          variant="outline"
          className="h-auto flex-col gap-8 rounded-xl border-2 border-primary/50 bg-primary/25 py-6 transition duration-150 hover:scale-105 hover:bg-primary hover:text-white"
        >
          Go Back Home
        </Button>
      </Link>
    </div>
  );
};
