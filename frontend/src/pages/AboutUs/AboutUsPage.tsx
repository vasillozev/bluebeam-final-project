import { ItemCard } from "@/components/App/ItemCard";
import { Link } from "react-router-dom";

export const AboutUsPage: React.FC = () => {
  return (
    <div className="container mb-20 flex w-full flex-col items-center justify-center gap-y-16">
      <h1 className="mt-6 text-center text-4xl font-black sm:text-4xl md:text-left lg:text-5xl">
        More about us
      </h1>
      <div className="flex w-full flex-col items-center gap-y-8">
        <div className="flex flex-col items-center">
          <h2 className="text-3xl font-black sm:text-3xl md:mt-0 md:text-left lg:text-4xl">
            Executive team
          </h2>
          <p className="font-500 text-lg">
            Meet the team of experienced industry leaders driving{" "}
            <Link to="/qr">our</Link> global vision
          </p>
        </div>

        <div className="grid w-full grid-cols-1 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 2xl:grid-cols-4">
          <ItemCard
            title={"Vasil Lozev"}
            description={"Developer"}
            image="/Vasil.jpg"
            customImageHeight={"200px"}
          />

          <ItemCard
            title={"Izabel Tasheva"}
            description={"Developer"}
            image="/IzabelPhoto.jpg"
            customImageHeight={"200px"}
          />

          <ItemCard
            title={"Dimo Dimov"}
            description={"Developer"}
            image="/DimoPhoto.jpg"
            customImageHeight={"200px"}
          />

          <ItemCard
            title={"Rado Kolev"}
            description={"Developer"}
            image="/Rado.jpg"
            customImageHeight={"200px"}
          />
        </div>
      </div>

      <div className="flex flex-col items-center">
        <h2 className="mb-1 text-3xl font-black sm:text-3xl md:mt-0 md:text-left lg:text-4xl">
          Architecture
        </h2>
        <p className="font-500 max-w-[75%] text-center text-lg md:mb-4">
          Our team of architects and engineers are experts in the field. We have
          extensive experience in the construction industry.
        </p>
      </div>
      <img
        className="-mb-[425px] -mt-36 hidden w-full rounded-xl opacity-90 lg:block"
        src="/solaris_diagram.png"
        alt="sketch photo"
      />

      <div className="flex w-full flex-col items-center gap-y-6">
        <div className="flex flex-col items-center">
          <h2 className="mb-1 text-center text-3xl font-black sm:text-3xl md:mt-0 md:text-left lg:text-4xl">
            Our mission and vision
          </h2>
          <p className="font-500 max-w-[75%] text-center text-lg">
            Solaris empowers those who build our world. Our mission is to
            delight customers with innovation and a simple, elegant experience.
            Our vision is to enable a sustainable and efficient future by
            simplifying the widespread adoption of digital technologies for
            those who build.
          </p>
        </div>

        <img
          className="hidden max-h-[75dvh] rounded-xl opacity-90 md:block"
          src="/facilityManager.jpeg"
          alt="facility-manager-and-workers"
        />
      </div>
      <div className="flex w-full flex-col items-center gap-y-6 md:flex-row md:gap-x-8">
        <div className="mr-0 flex w-full flex-col gap-y-2 md:mr-20">
          <h2 className="mb-1 text-center text-2xl font-black sm:text-3xl md:mt-0 md:text-left lg:text-4xl">
            The Nemetschek Group
          </h2>
          <p className="font-500 text-center md:text-left">
            We are a proud member of the Nemetschek Groups Build and
            Construction division, a global pioneer in the digital construction
            space with software solutions covering the entire project lifecycle.
            Together with our sister brands, we are driving digital
            transformation across the global AECO industry with a shared
            commitment to open standards and continual innovation.
          </p>
        </div>
        <img
          className="hidden max-h-[500px] rounded-xl opacity-90 md:block"
          src="/buildingaboutus.webp"
          alt="sketch photo"
        ></img>
      </div>
    </div>
  );
};
