import { UserContext } from "@/components/Context/UserContext";
import React, { useContext, useEffect, useState } from "react";

import { CreateFacility } from "@/components/FacilityDialog/FacilityDialog";
import { Mapbox } from "@/components/Mapbox/Mapbox";
import Spinner from "@/components/ui/spinner";

import {
  FacilityResponse,
  getAllWithIssuesCount,
} from "@/services/facility.service";
import { getErrorMessage } from "@/utils/getErrorMessage";
import { toast } from "sonner";

export const MapPage: React.FC = () => {
  const [lat, setLat] = useState<undefined | number>(undefined);
  const [lng, setLng] = useState<undefined | number>(undefined);
  const [zoom, setZoom] = useState<undefined | number>(undefined);
  const [facilities, setFacilities] = useState<FacilityResponse[]>([]);
  const [loading, setLoading] = useState(true);
  const { isManager } = useContext(UserContext);

  // Temporary filtering as we're using dummy f.lat and f.lng data. Remove filter after fully implemented.
  useEffect(() => {
    getAllWithIssuesCount()
      .then((fetchedFacilities) =>
        fetchedFacilities.filter((f) => {
          return f.lat > -90 && f.lat < 90 && f.lng > -180 && f.lng < 180;
        }),
      )
      .then((fetchedFacilities) => setFacilities(fetchedFacilities))
      .catch((e) => toast.error(getErrorMessage(e)))
      .finally(() => setLoading(false));
  }, []);

  const addFacility = (newFacility: FacilityResponse) => {
    setFacilities((prevFacilities) => [...prevFacilities, newFacility]);
    setLat(newFacility.lat);
    setLng(newFacility.lng);
    setZoom(14);
  };

  if (loading) {
    return <Spinner />;
  }

  return (
    <div className="container h-[75dvh]">
      <div className="mb-2 flex w-full items-center justify-between">
        <h2 className="text-3xl font-semibold">Facilities Map</h2>
        {isManager() && <CreateFacility addFacility={addFacility} />}
      </div>
      <Mapbox
        facilities={facilities}
        lat={lat}
        lng={lng}
        zoom={zoom}
        managerView={isManager()}
      />
    </div>
  );
};
