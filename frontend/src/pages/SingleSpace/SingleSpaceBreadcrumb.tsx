import React from "react";

import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbList,
  BreadcrumbSeparator,
} from "@/components/ui/breadcrumb";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { cn } from "@/lib/utils";
import { Link } from "react-router-dom";
import { CheckIcon, ChevronDownIcon } from "lucide-react";
import { SpaceResponse } from "@/services/space.service";

interface SpaceBreadcrumbProps {
  facilityId: string;
  facilityName: string;
  spaceName: string;
  spaces: SpaceResponse[];
}

export const SingleSpaceBreadcrumb: React.FC<SpaceBreadcrumbProps> = ({
  facilityId,
  facilityName,
  spaceName,
  spaces,
}) => {
  return (
    <Breadcrumb>
      <BreadcrumbList>
        <BreadcrumbItem className="hidden sm:block">
          <Link to={"/"}>Home</Link>
        </BreadcrumbItem>
        <BreadcrumbSeparator className="hidden sm:block" />
        <BreadcrumbItem>
          <Link
            to={`/${encodeURIComponent(facilityId + "~" + facilityName)}/spaces`}
          >
            {facilityName}
          </Link>
        </BreadcrumbItem>
        <BreadcrumbSeparator />
        <BreadcrumbItem>
          <DropdownMenu modal={false}>
            <DropdownMenuTrigger className="flex items-center gap-1">
              {spaceName}
              <ChevronDownIcon size={14} />
            </DropdownMenuTrigger>
            {spaces.length > 0 && (
              <DropdownMenuContent
                className="scrollbar max-h-96 overflow-y-auto"
                align="start"
              >
                {spaces?.map((s) => (
                  <Link
                    key={s.id}
                    to={`/${encodeURIComponent(facilityId + "~" + facilityName)}/spaces/${s.id}`}
                  >
                    <DropdownMenuItem>
                      {s.name}
                      <CheckIcon
                        className={cn(
                          "ml-auto h-4 w-4",
                          s.name === spaceName ? "opacity-100" : "opacity-0",
                        )}
                      />
                    </DropdownMenuItem>
                  </Link>
                ))}
              </DropdownMenuContent>
            )}
          </DropdownMenu>
        </BreadcrumbItem>
      </BreadcrumbList>
    </Breadcrumb>
  );
};
