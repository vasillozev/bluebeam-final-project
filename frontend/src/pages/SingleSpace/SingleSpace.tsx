import React, { useEffect, useState } from "react";

import { Button } from "@/components/ui/button";
import { PlusIcon } from "lucide-react";
import { useParams } from "react-router-dom";

import {
  getBySpace,
  IssueLocation,
  IssueResponse,
} from "@/services/issue.service";
import { getByFacility, SpaceResponse } from "@/services/space.service";
import { getErrorMessage } from "@/utils/getErrorMessage";

import { CreateIssueDrawer } from "@/components/IssueDrawer/CreateIssueDrawer";
import { ViewIssueDrawer } from "@/components/IssueDrawer/ViewIssueDrawer";
import Spinner from "@/components/ui/spinner";
import { Status } from "@/utils/enums/status";
import { toast } from "sonner";
import { IssueMap } from "../../components/IssueMap/IssueMap";
import { SingleSpaceBreadcrumb } from "./SingleSpaceBreadcrumb";
export const SingleSpace: React.FC = () => {
  const [newIssue, setNewIssue] = useState<IssueLocation | null>(null);
  const [issues, setIssues] = useState<IssueResponse[]>([]);
  const [selectedIssue, setSelectedIssue] = useState<IssueResponse | null>(
    null,
  );
  const [space, setSpace] = useState<SpaceResponse | null>(null);
  const [isAddingIssue, setIsAddingIssue] = useState<boolean>(false);
  const [loading, setLoading] = useState(true);
  const [openCreateDrawer, setOpenCreateDrawer] = useState(false);
  const [openViewDrawer, setOpenViewDrawer] = useState(false);
  const [spaces, setSpaces] = useState<SpaceResponse[]>([]);
  const { facilityParams, spaceId } = useParams<{
    facilityParams: string;
    spaceId: string;
  }>();
  const [facilityId, facilityName] = facilityParams!.split("~");

  // fetch all spaces (needed for breadcrumb dropdown)
  useEffect(() => {
    getByFacility(facilityId)
      .then((s) => {
        const current = s.find((s) => s.id == spaceId!);
        setSpace(current!);

        return s;
      })
      .then((s) => setSpaces(s))
      .catch((e) => toast.error(getErrorMessage(e)))
      .finally(() => setLoading(false));
  }, [facilityId, spaceId]);

  // fetch issues
  useEffect(() => {
    getBySpace(spaceId!)
      .then((issues) => issues.filter((i) => i.statusId !== Status.DONE)) // maybe do that in the backend in the future
      .then((issues) => setIssues(issues))
      .catch((e) => toast.error(getErrorMessage(e)));
  }, [spaceId]);

  const handleToggleMode = () => {
    setNewIssue(null);
    setIsAddingIssue((prevState) => !prevState);
  };

  const handleViewIssue = (issue: IssueResponse) => {
    if (!isAddingIssue) {
      setOpenViewDrawer(true);
      setSelectedIssue(issue);
    }
  };

  const handleMarkLocation = (event: React.MouseEvent<HTMLDivElement>) => {
    if (isAddingIssue) {
      const rect = event.currentTarget.getBoundingClientRect();
      const coordX = ((event.clientX - rect.left) / rect.width) * 100 - 1.2;
      const coordY = ((event.clientY - rect.top) / rect.height) * 100 - 2;

      const currentIssue: IssueLocation = {
        coordX,
        coordY,
      };

      setNewIssue(currentIssue);
      setIsAddingIssue(false);
      setOpenCreateDrawer(true);
    }
  };

  if (loading) {
    return <Spinner />;
  }

  return (
    <div className="container">
      <div className="mb-3 flex w-full items-center justify-between">
        <div>
          <h2 className="mb-2 text-xl font-semibold md:text-2xl lg:text-3xl">
            {space?.name}
          </h2>
          <SingleSpaceBreadcrumb
            facilityId={facilityId}
            facilityName={facilityName}
            spaceName={space!.name}
            spaces={spaces}
          />
        </div>
        <Button
          variant="ghost"
          className={isAddingIssue ? `bg-accent text-accent-foreground` : ""}
          onClick={handleToggleMode}
        >
          {isAddingIssue ? "Mark issue" : <PlusIcon />}
        </Button>
      </div>

      <IssueMap
        issues={issues}
        blueprintUrl={space?.blueprintUrl}
        onView={handleViewIssue}
        onAdd={handleMarkLocation}
        isAddingIssue={isAddingIssue}
        newIssue={newIssue}
      />

      <ViewIssueDrawer
        openDrawer={openViewDrawer}
        setOpenDrawer={setOpenViewDrawer}
        selectedIssue={selectedIssue}
        setSelectedIssue={setSelectedIssue}
        setIssues={setIssues}
      />

      <CreateIssueDrawer
        setIssues={setIssues}
        facilityId={facilityId}
        spaceId={spaceId}
        openDrawer={openCreateDrawer}
        newIssue={newIssue}
        setOpenDrawer={setOpenCreateDrawer}
        setNewIssue={setNewIssue}
      />
    </div>
  );
};

export default SingleSpace;
