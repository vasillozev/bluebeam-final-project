import { STATUS_LABELS as statuses } from "@/utils/enums/status";
import { ViewIssueDrawer } from "@/components/IssueDrawer/ViewIssueDrawer";
import { FacilityResponse } from "@/services/facility.service";
import { IssueResponse, getAll, update } from "@/services/issue.service";
import { SpaceResponse } from "@/services/space.service";
import { getErrorMessage } from "@/utils/getErrorMessage";
import { capitalizeAllLetters } from "@/utils/string";
import { useEffect, useState } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { FilterCombobox } from "./FilterCombobox";
import { useContext } from "react";
import { UserContext } from "../../components/Context/UserContext";
import { toast } from "sonner";
import { IssueCard } from "./IssueCard";

export const IssuesPage: React.FC = () => {
  const [spaceFilter, setSpaceFilter] = useState("");
  const [facilityFilter, setFacilityFilter] = useState("");
  const [issues, setIssues] = useState<IssueResponse[]>([]);
  const [spaces, setSpaces] = useState<SpaceResponse[]>([]);
  const [facilities, setFacilities] = useState<FacilityResponse[]>([]);
  const [currentHover, setCurrentHover] = useState<number | null>(null);
  const [openViewDrawer, setOpenViewDrawer] = useState(false);
  const [selectedIssue, setSelectedIssue] = useState<IssueResponse | null>(
    null,
  );
  const { user, isManager } = useContext(UserContext);
  const [onlyMyIssuesFilter, setOnlyMyIssuesFilter] = useState(false);
  const [filteredIssues, setFilteredIssues] = useState<IssueResponse[]>([]);
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();

  useEffect(() => {
    if (searchParams.has("f")) {
      const facilityParam = searchParams.get("f");
      facilityParam && setFacilityFilter(facilityParam);
    }

    if (searchParams.has("s")) {
      const spaceParam = searchParams.get("s");
      spaceParam && setSpaceFilter(spaceParam);
    }

    if (searchParams.has("m")) {
      const assigneeParams = searchParams.get("m");
      assigneeParams && setOnlyMyIssuesFilter(assigneeParams === "true");
    }
  }, [searchParams]);

  const handleViewIssue = (issue: IssueResponse) => {
    setOpenViewDrawer(true);
    setSelectedIssue(issue);
  };

  // issue.id, timeout (has to be in a useState otherwise it's wiped when you move a issue)
  const [, setIssueUpdates] = useState<Record<number, NodeJS.Timeout>>({});

  useEffect(() => {
    getAll(true)
      .then((issues) => {
        setIssues(issues);

        // get all spaces, .reduce to deduplicate them
        const spaces = issues
          .map((issue) => issue.space)
          .reduce((acc, curr) => {
            if (curr && !acc.some((space) => space.id === curr?.id)) {
              const hasOpenIssues = issues.some((s) => s.spaceId === curr?.id);
              if (hasOpenIssues) {
                acc.push(curr);
              }
            }
            return acc;
          }, [] as SpaceResponse[]);

        setSpaces(spaces);

        // get all facilities, .reduce to deduplicate them
        const facilities = issues
          .map((issue) => issue.facility)
          .reduce((acc, curr) => {
            if (curr && !acc.some((facility) => facility.id === curr?.id)) {
              const hasOpenIssues = issues.some(
                (s) => s.facilityId === curr?.id,
              );
              if (hasOpenIssues) {
                acc.push(curr);
              }
            }
            return acc;
          }, [] as FacilityResponse[]);

        setFacilities(facilities);
      })
      .catch((e) => toast.error(getErrorMessage(e)));
  }, []);

  const handleFilterIssuesForMe = () => {
    setOnlyMyIssuesFilter(!onlyMyIssuesFilter);
  };

  useEffect(() => {
    let filtered = issues;
    const params = new URLSearchParams();

    if (onlyMyIssuesFilter) {
      filtered = filtered.filter((issue) => issue.assigneeId === user?.id);
      params.append("m", onlyMyIssuesFilter.toString());
    } else {
      params.delete("m");
    }

    if (facilityFilter) {
      filtered = filtered.filter(
        (issue) => issue.facilityId.toString() === facilityFilter,
      );

      params.append("f", facilityFilter);
    } else {
      params.delete("f");
    }

    if (spaceFilter) {
      filtered = filtered.filter(
        (issue) => issue.spaceId.toString() === spaceFilter,
      );

      params.append("s", spaceFilter);
    } else {
      params.delete("s");
    }

    navigate(`?${params.toString()}`);
    setFilteredIssues(filtered);
  }, [
    onlyMyIssuesFilter,
    facilityFilter,
    spaceFilter,
    issues,
    user?.id,
    navigate,
  ]);

  const updateIssue = async (issue: IssueResponse) => {
    try {
      await update({
        id: issue.id,
        statusId: issue.statusId,
        resolvedAt:
          issue.statusId == 2
            ? new Date().toLocaleString("en-GB", {
                dateStyle: "medium",
                timeStyle: "medium",
              })
            : null,
      });
    } catch (e) {
      toast.error(getErrorMessage(e));
    }
  };

  const handleDragEnter = (status: number) => {
    setCurrentHover(status);
  };

  const handleDrop = (
    e: React.DragEvent<HTMLDivElement>,
    newStatus: number,
  ) => {
    const issueId = e.dataTransfer.getData("issueId");
    setIssues((prevIssues) => {
      const issueIdx = prevIssues.findIndex((issue) => issue.id === +issueId);
      if (issueIdx === -1) {
        console.error(`issue with id ${issueId} not found`);
        return prevIssues;
      }

      const issue = {
        ...prevIssues[issueIdx],
        statusId: newStatus,
        resolvedAt:
          newStatus == 2
            ? new Date().toLocaleString("en-GB", {
                dateStyle: "medium",
                timeStyle: "medium",
              })
            : null,
      };

      prevIssues[issueIdx] = issue;

      // if there's already an update request, cancel it and replace with a new one
      setIssueUpdates((updates) => {
        const timeout = updates[issue.id];
        if (timeout) {
          delete updates[issue.id];
          clearTimeout(timeout);
        }

        updates[issue.id] = setTimeout(() => updateIssue(issue), 750);

        return updates;
      });

      return prevIssues;
    });
    setCurrentHover(null);
  };

  const handleClearFilters = () => {
    setFacilityFilter("");
    setSpaceFilter("");
    setOnlyMyIssuesFilter(false);
  };

  const handleSelectFacilityFilter = (value: string) => {
    if (value == facilityFilter) {
      setFacilityFilter("");
      setSpaceFilter("");
    } else {
      setFacilityFilter(value);
    }
  };

  return (
    <div className="w-full pl-2">
      <div className="mb-8 flex w-full flex-col items-center justify-between md:flex-row">
        <h2 className="text-xl font-semibold md:text-2xl lg:text-3xl">
          Issues
        </h2>
        <div className="mt-2 flex flex-col-reverse gap-x-4 pt-1 md:flex-row">
          <div className="mb-0 flex flex-col items-center justify-center gap-2 md:mb-0 md:flex-row">
            {(onlyMyIssuesFilter || facilityFilter || spaceFilter) && (
              <button onClick={handleClearFilters} className="font-semibold">
                Clear filters
              </button>
            )}
            {!isManager() && (
              <button
                onClick={handleFilterIssuesForMe}
                className={`w-[13ch] font-semibold`}
              >
                {onlyMyIssuesFilter ? "Show all issues" : "Only my issues"}
              </button>
            )}
          </div>
          <div className="mb-2 flex justify-between gap-2 md:mb-0">
            <FilterCombobox
              items={facilities.map(({ name, id }) => ({
                label: name,
                value: id.toString(),
              }))}
              value={facilityFilter}
              setValue={handleSelectFacilityFilter}
              title="Filter by facility"
              placeholder="Select your facility"
            />
            <FilterCombobox
              disabled={!facilityFilter}
              items={spaces
                .filter(
                  (space) => space.facilityId.toString() === facilityFilter,
                )
                .map(({ name, id }) => ({
                  label: name,
                  value: id.toString(),
                }))}
              value={spaceFilter}
              setValue={(value) => {
                // if selected is the same, deselect it instead
                setSpaceFilter(value === spaceFilter ? "" : value);
              }}
              title="Filter by space"
              placeholder="Select your space"
            />
          </div>
        </div>
      </div>

      <div className="overflow-auto">
        <div className="mb-10 grid h-[60dvh] min-w-[990px] flex-1 grid-cols-3 md:h-[66dvh]">
          {statuses.map(capitalizeAllLetters).map((status, statusIdx) => (
            <div
              key={statusIdx}
              onDrop={(e) => handleDrop(e, statusIdx)}
              onDragOver={(e) => e.preventDefault()}
              onDragEnter={() => handleDragEnter(statusIdx)}
              className="flex min-w-[330px] flex-col border-l px-4 first:border-0 first:pl-0 last:pr-0"
            >
              <span className="pb-2 text-xl font-semibold">{status}</span>
              <div
                className={`h-full ${
                  currentHover == statusIdx ? "bg-foreground/10" : ""
                }`}
              >
                {filteredIssues
                  .filter((issue) => issue.statusId === statusIdx)
                  .map((issue) => (
                    <IssueCard
                      key={issue.id}
                      issue={issue}
                      handleView={handleViewIssue}
                    />
                  ))}
              </div>
            </div>
          ))}
        </div>
      </div>

      <ViewIssueDrawer
        openDrawer={openViewDrawer}
        setOpenDrawer={setOpenViewDrawer}
        selectedIssue={selectedIssue}
        setSelectedIssue={setSelectedIssue}
        setIssues={setIssues}
      />
    </div>
  );
};
