import { useContext } from "react";

import { Route, Routes } from "react-router-dom";

import { UserContext } from "./components/Context/UserContext.tsx";
import Layout from "./components/Layout/Layout.tsx";
import { FacilitiesPage } from "./pages/Facilities";
import { HomePage } from "./pages/Home";
import { SpacesPage } from "./pages/Spaces";

import "./index.css";
import { AboutUsPage } from "./pages/AboutUs/AboutUsPage.tsx";
import { NotFound } from "./pages/NotFound/NotFound.tsx";
import { QRPage } from "./pages/QR/QRPage.tsx";
import { SingleIssueDetails } from "./pages/SingleIssueDetails/SingleIssueDetails.tsx";
import SingleSpace from "./pages/SingleSpace/SingleSpace.tsx";
import { IssuesPage, MapPage } from "./pages/index.ts";

function App() {
  const { isManager } = useContext(UserContext);
  return (
    <Layout>
      <Routes>
        <Route index element={<HomePage />} />
        <Route path="/map" element={<MapPage />} />
        <Route path="/qr" element={<QRPage />} />
        <Route path="/issues" element={<IssuesPage />} />
        <Route path="/issues/:issueId" element={<SingleIssueDetails />} />
        <Route path="/aboutus" element={<AboutUsPage />} />

        {isManager() && (
          <>
            <Route path="/facilities" element={<FacilitiesPage />} />
            <Route
              path="/:facilityParams/spaces"
              element={<SpacesPage />}
            ></Route>
            <Route
              path="/:facilityParams/spaces/:spaceId"
              element={<SingleSpace />}
            />
          </>
        )}
        <Route path="*" element={<NotFound />} />
      </Routes>
    </Layout>
  );
}

export default App;
