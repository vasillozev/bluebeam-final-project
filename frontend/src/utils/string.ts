export function capitalizeFirstLetter(string: string) {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

export function capitalizeAllLetters(string: string) {
  return string.split(" ").map(capitalizeFirstLetter).join(" ");
}

export function removeSpaces(string: string) {
  return string.replace(/\s/g, "");
}
