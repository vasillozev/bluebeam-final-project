import { Address } from "@/components/AddressInput";
import {
  FILE_EXTENSIONS,
  FILE_MAX_SIZE_IN_BYTES,
  FILE_MAX_SIZE_IN_MB,
  LENGTH_VALIDATION,
} from "./constants";

export const validateName = (name: string) => {
  let error = "";

  if (!name) {
    error = "The name field is required!";
    return error;
  }

  if (
    name.length < LENGTH_VALIDATION.NAME_MIN_LENGTH ||
    name.length > LENGTH_VALIDATION.NAME_MAX_LENGTH
  ) {
    error = `The name must be between ${LENGTH_VALIDATION.NAME_MIN_LENGTH} and ${LENGTH_VALIDATION.NAME_MAX_LENGTH} characters!`;
    return error;
  }

  return error;
};

export const validateAddress = (
  address: Address | null,
  coordinatesRequired = true,
) => {
  let error = "";

  if (!address) {
    error = "The address field is required!";
    return error;
  }

  // if coordinates arent required we return early
  if (!coordinatesRequired) {
    return error;
  } else {
    if (!address.coordinates) {
      error = "Please select a valid address, no coordinates found!";
      return error;
    }

    if (
      address.coordinates.latitude < -90 ||
      address.coordinates.latitude > 90
    ) {
      error = "The latitude must be between -90 and 90!";
      return error;
    }

    if (
      address.coordinates.longitude < -180 ||
      address.coordinates.longitude > 180
    ) {
      error = "The longitude must be between -180 and 180!";
      return error;
    }
  }

  return error;
};

export const validateDescription = (description: string) => {
  let error = "";

  if (!description) {
    error = "The description field is required!";
    return error;
  }

  if (
    description.length < LENGTH_VALIDATION.DESCRIPTION_MIN_LENGTH ||
    description.length > LENGTH_VALIDATION.DESCRIPTION_MAX_LENGTH
  ) {
    error = `The description must be between ${LENGTH_VALIDATION.DESCRIPTION_MIN_LENGTH} and ${LENGTH_VALIDATION.DESCRIPTION_MAX_LENGTH} characters!`;
    return error;
  }

  return error;
};
export const validateFile = (file: File | null, isRequired = true) => {
  let error = "";

  if (!file) {
    // if file is not required, we ignore it's null
    error = isRequired ? "The file field is required!" : "";
    return error;
  }

  if (!FILE_EXTENSIONS.includes(file.type)) {
    error = `The allowed file formats are ${FILE_EXTENSIONS.join(", ")}!`;
    return error;
  }

  if (file.size > FILE_MAX_SIZE_IN_BYTES) {
    error = `File can't be larger than ${FILE_MAX_SIZE_IN_MB} MB!`;
    return error;
  }

  return error;
};
