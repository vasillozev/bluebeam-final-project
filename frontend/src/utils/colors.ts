export const statusBackgroundColorsIssueMap = [
  "bg-red-400/25",
  "bg-cyan-400/25",
  "bg-lime-400/25",
] as const;

export const statusBorderColors = [
  "border-red-400/75",
  "border-cyan-400/75",
  "border-lime-400/75",
] as const;

export const statusBackgroundColors = [
  "bg-red-400/10",
  "bg-cyan-400/10",
  "bg-lime-400/10",
] as const;

export const statusHoverBorderColors = [
  "hover:border-red-400",
  "hover:border-cyan-300",
  "hover:border-lime-300",
] as const;
