export const STATUS_LABELS = Object.freeze(["TO-DO", "IN PROGRESS", "DONE"]);

// note: these indexes are also used in the backend
export enum Status {
  TODO = 0,
  IN_PROGRESS = 1,
  DONE = 2,
}

export function getStatusLabel(status: Status) {
  return STATUS_LABELS[status];
}
