import { useTheme } from "next-themes";
import { Toaster as Sonner } from "sonner";

type ToasterProps = React.ComponentProps<typeof Sonner>;

const Toaster = ({ ...props }: ToasterProps) => {
  const { theme } = useTheme();

  return (
    <Sonner
      theme={theme as ToasterProps["theme"]}
      className="toaster group"
      toastOptions={{
        classNames: {
          description: "group-[.toast]:text-muted-foreground",
          actionButton:
            "group-[.toast]:bg-primary group-[.toast]:text-primary-foreground",
          cancelButton: "group-[.toast]:bg-white group-[.toast]:text-black",
          default: "border",
          error:
            "group toast group-[.toaster]:bg-rose group-[.toaster]:text-rose-400 dark:group-[.toaster]:text-foreground group-[.toaster]:shadow-lg",
          success:
            "group toast group-[.toaster]:bg-green group-[.toaster]:text-green-400 dark:group-[.toaster]:text-foreground group-[.toaster]:shadow-lg",
          warning:
            "group toast group-[.toaster]:bg-orange group-[.toaster]:text-orange-400 dark:group-[.toaster]:text-foreground group-[.toaster]:shadow-lg",
          info: "group toast group-[.toaster]:bg-sky group-[.toaster]:text-sky-400 dark:group-[.toaster]:text-foreground group-[.toaster]:shadow-lg",
        },
      }}
      {...props}
    />
  );
};

export { Toaster };
