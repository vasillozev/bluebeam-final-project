import { LoaderCircle } from "lucide-react";

const Spinner = () => {
  return <LoaderCircle className="my-auto size-12 animate-spin" />;
};

export default Spinner;
