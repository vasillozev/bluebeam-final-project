import users from "@/data/users.json";
import { IssueResponse, update } from "@/services/issue.service";
import { STATUS_LABELS } from "@/utils/enums/status";
import { getErrorMessage } from "@/utils/getErrorMessage";
import { capitalizeAllLetters } from "@/utils/string";
import { PopoverTrigger } from "@radix-ui/react-popover";
import { Image } from "lucide-react";
import { useContext } from "react";
import { Link } from "react-router-dom";
import { toast } from "sonner";
import { UserContext } from "../Context/UserContext";
import { Popover, PopoverContent } from "../ui/popover";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "../ui/select";
import { Separator } from "../ui/separator";
import { Sheet, SheetContent, SheetHeader, SheetTitle } from "../ui/sheet";

interface IViewIssueDrawerProps {
  openDrawer: boolean;
  setOpenDrawer: React.Dispatch<React.SetStateAction<boolean>>;
  selectedIssue: IssueResponse | null;
  setSelectedIssue: React.Dispatch<React.SetStateAction<IssueResponse | null>>;
  setIssues: React.Dispatch<React.SetStateAction<IssueResponse[]>>;
}

export const ViewIssueDrawer: React.FC<IViewIssueDrawerProps> = ({
  openDrawer,
  setOpenDrawer,
  selectedIssue,
  setSelectedIssue,
  setIssues,
}) => {
  const { user, isManager } = useContext(UserContext);

  const handleOpenChange = (openDrawer: boolean) => {
    setOpenDrawer(openDrawer);
  };

  const handleAssignToMe = async () => {
    if (user && user.id && selectedIssue) {
      setSelectedIssue({ ...selectedIssue, assigneeId: user.id });

      setIssues((prevIssues) =>
        prevIssues.map((i) =>
          i.id === selectedIssue.id ? { ...i, assigneeId: user.id! } : i,
        ),
      );

      await update({
        id: selectedIssue.id,
        assigneeId: user.id,
      }).catch((e) => toast.error(getErrorMessage(e)));
    }
  };

  const handleAssignToNoOne = async () => {
    if (selectedIssue) {
      setSelectedIssue({ ...selectedIssue, assigneeId: null });

      setIssues((prevIssues) =>
        prevIssues.map((i) =>
          i.id === selectedIssue.id ? { ...i, assigneeId: null } : i,
        ),
      );

      await update({
        id: selectedIssue.id,
        assigneeId: undefined,
      }).catch((e) =>
        console.error("Error creating space", getErrorMessage(e)),
      );
    }
  };

  const handleStatusChange = async (statusId: string) => {
    if (selectedIssue) {
      setSelectedIssue({
        ...selectedIssue,
        statusId: +statusId,
        resolvedAt:
          +statusId == 2
            ? new Date().toLocaleString("en-GB", {
                dateStyle: "medium",
                timeStyle: "medium",
              })
            : null,
      });

      setIssues((prevIssues) =>
        prevIssues.map((i) =>
          i.id === selectedIssue.id
            ? {
                ...i,
                statusId: +statusId,
                resolvedAt:
                  +statusId == 2
                    ? new Date().toLocaleString("en-GB", {
                        dateStyle: "medium",
                        timeStyle: "medium",
                      })
                    : null,
              }
            : i,
        ),
      );

      await update({
        id: selectedIssue.id,
        statusId: +statusId,
        resolvedAt:
          +statusId == 2
            ? new Date().toLocaleString("en-GB", {
                dateStyle: "medium",
                timeStyle: "medium",
              })
            : null,
      }).catch((e) => toast.error(getErrorMessage(e)));
    }
  };

  const handleAssigneeChange = async (assigneeId: string) => {
    if (selectedIssue) {
      if (assigneeId === "None") {
        await handleAssignToNoOne();
      } else {
        setSelectedIssue({ ...selectedIssue, assigneeId: +assigneeId });

        setIssues((prevIssues) =>
          prevIssues.map((i) =>
            i.id === selectedIssue.id ? { ...i, assigneeId: +assigneeId } : i,
          ),
        );

        await update({
          id: selectedIssue.id,
          assigneeId: +assigneeId,
        }).catch((e) => toast.error(getErrorMessage(e)));
      }
    }
  };

  return (
    <Sheet open={openDrawer} onOpenChange={handleOpenChange}>
      <SheetContent onOpenAutoFocus={(e) => e.preventDefault()} side={"right"}>
        <SheetHeader>
          <SheetTitle className="text-left">
            <Link to={`/issues/${selectedIssue?.id}`} className="text-blue-500">
              {selectedIssue?.title}
            </Link>
          </SheetTitle>
        </SheetHeader>
        <div className="text-left text-popover-foreground">
          <div className="mt-2 flex flex-col justify-between md:flex-row">
            <div className="flex-col">
              <p className="mb-1">Status:</p>
              <Select
                value={String(selectedIssue?.statusId)}
                onValueChange={handleStatusChange}
              >
                <SelectTrigger className="h-[30px] w-[170px]">
                  <SelectValue placeholder="Select a status" />
                </SelectTrigger>
                <SelectContent>
                  {STATUS_LABELS.map(capitalizeAllLetters).map(
                    (status, statusIdx) => (
                      <SelectItem value={String(statusIdx)} key={statusIdx}>
                        {status}
                      </SelectItem>
                    ),
                  )}
                </SelectContent>
              </Select>
              <p className="my-1">Assignee:</p>
              <Select
                value={
                  selectedIssue?.assigneeId?.toString()
                    ? selectedIssue.assigneeId.toString()
                    : "None"
                }
                onValueChange={handleAssigneeChange}
              >
                <SelectTrigger className="h-[30px] w-[170px]">
                  <SelectValue placeholder="Select an assignee" />
                </SelectTrigger>
                <SelectContent>
                  <SelectItem value={"None"} key={"None"}>
                    None
                  </SelectItem>
                  {users.map((user) => (
                    <SelectItem value={user.id.toString()} key={user.id}>
                      {user.name}
                    </SelectItem>
                  ))}
                </SelectContent>
              </Select>
              {!isManager() && (
                <p
                  onClick={handleAssignToMe}
                  className="my-1 cursor-pointer text-sm text-blue-500"
                >
                  Assign to me
                </p>
              )}
            </div>
            <div className="mt-2 text-sm md:mt-1">
              <p>Created At:</p>
              {selectedIssue?.createdAt &&
                new Date(selectedIssue?.createdAt).toLocaleString("en-GB", {
                  dateStyle: "short",
                  timeStyle: "short",
                })}

              {selectedIssue?.resolvedAt &&
                selectedIssue?.statusId == 2 &&
                new Date(selectedIssue.resolvedAt).getFullYear() > 1000 && (
                  <>
                    <p className="mt-1">Resolved At:</p>
                    <p>
                      {new Date(selectedIssue.resolvedAt).toLocaleString(
                        "en-GB",
                        {
                          dateStyle: "short",
                          timeStyle: "short",
                        },
                      )}
                    </p>
                  </>
                )}
            </div>
          </div>
          <Separator className="my-4" />
          <p className="mb-4">{selectedIssue?.description}</p>
          {selectedIssue?.photoUrl && (
            <Popover>
              <PopoverTrigger className="flex flex-row items-center gap-1 text-base text-blue-500">
                <Image size={20} /> View Image
              </PopoverTrigger>
              <PopoverContent>
                <img src={selectedIssue?.photoUrl} alt={"photo-of-issue"} />
              </PopoverContent>
            </Popover>
          )}
        </div>
      </SheetContent>
    </Sheet>
  );
};
