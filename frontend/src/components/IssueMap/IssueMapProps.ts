import { IssueLocation, IssueResponse } from "@/services/issue.service";

export type IssueMapProps = {
  issues: IssueResponse[];
  blueprintUrl: string | undefined;
  onView?: (issue: IssueResponse) => void;
  onAdd?: (event: React.MouseEvent<HTMLDivElement>) => void;
  newIssue?: IssueLocation | null;
  isAddingIssue?: boolean;
};
