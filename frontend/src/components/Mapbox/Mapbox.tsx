import { FacilityResponse } from "@/services/facility.service";
import mapboxgl from "mapbox-gl";
import { useTheme } from "next-themes";
import React, { useEffect, useRef } from "react";
import { createRoot } from "react-dom/client";
import { useNavigate } from "react-router-dom";
import { MapboxPopup } from "./MapboxPopup";

interface MapboxProps {
  lng?: number;
  lat?: number;
  zoom?: number;
  facilities: FacilityResponse[];
  managerView: boolean;
}

// Default coords for Sofia, could pass optional coords for specific facilities
export const Mapbox: React.FC<MapboxProps> = ({
  lng = 23.324455,
  lat = 42.69194,
  zoom = 12,
  facilities,
  managerView,
}) => {
  const { theme } = useTheme();
  const map = useRef<mapboxgl.Map | null>(null);
  const mapContainer = useRef(null);
  const navigate = useNavigate();

  mapboxgl.accessToken = import.meta.env.VITE_MAPBOX_KEY;

  useEffect(() => {
    if (map.current) {
      // if center is different we pan to the new coordinates
      const center = map.current.getCenter();
      if (center.lat !== lat || center.lng !== lng) {
        map.current.panTo({ lat, lng });

        center.lat = lat;
        center.lng = lng;

        map.current.setCenter(center);
      }

      return;
    } // initialize map only once

    if (!mapContainer.current) return; // abort if map container is not specified

    map.current = new mapboxgl.Map({
      container: mapContainer.current,
      style: "mapbox://styles/mapbox/streets-v12",
      center: [lng, lat],
      zoom: zoom,
    });
  }, [lat, lng, zoom, theme]);

  useEffect(() => {
    if (map.current) {
      for (const f of facilities) {
        if (!managerView && f.issuesCount === 0) continue;

        const popupContent = document.createElement("div");

        const root = createRoot(popupContent);
        root.render(
          <MapboxPopup
            id={f.id}
            name={f.name}
            url={
              managerView
                ? `/${encodeURIComponent(f.id + "~" + f.name)}/spaces`
                : `/issues?f=${encodeURIComponent(f.id)}`
            }
            navigate={navigate}
            photoUrl={f.photoUrl}
            address={f.address}
            issuesCount={f.issuesCount ? f.issuesCount : 0}
          />,
        );

        const popup = new mapboxgl.Popup({
          offset: 25,
          closeButton: false,
        }).setDOMContent(popupContent);

        const customMarker = document.createElement("div");
        customMarker.innerHTML = `<img src="${f.photoUrl}" class="h-10 w-10 object-cover rounded-full" />`;

        new mapboxgl.Marker(customMarker)
          .setLngLat([f.lng, f.lat])
          .setPopup(popup)
          .addTo(map.current!);
      }
    }
  }, [map, facilities, navigate, managerView]);

  useEffect(() => {
    if (theme === "dark") {
      map.current?.setStyle(
        "mapbox://styles/dimogenesis/clxagb7sh02co01qsd5qwd4kc",
      );
    } else {
      map.current?.setStyle("mapbox://styles/mapbox/streets-v12");
    }
  }, [theme]);

  return <div ref={mapContainer} className="h-full w-full" />;
};
