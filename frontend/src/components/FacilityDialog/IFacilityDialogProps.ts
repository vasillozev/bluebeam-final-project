import type { FacilityResponse } from "@/services/facility.service";

export type FacilityEditableFields = Pick<
  FacilityResponse,
  "address" | "name" | "photoUrl"
>;

export type FacilityModalPredefinedData = Partial<FacilityEditableFields> & {
  id: string;
};

export interface IFacilityDialogProps {
  addFacility: (facility: FacilityResponse) => void;

  onClose?: () => void;
  updateFacility?: (updated: FacilityModalPredefinedData) => void;
  predefinedData?: FacilityModalPredefinedData;
}
