import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import {
  validateDescription,
  validateFile,
  validateName,
} from "@/utils/validations";
import { Input } from "../ui/input";
import { Label } from "../ui/label";
import { Textarea } from "../ui/textarea";

import { SpaceBody, create, update } from "@/services/space.service";
import { getErrorMessage } from "@/utils/getErrorMessage";
import { PlusIcon } from "lucide-react";
import { useEffect, useState } from "react";
import { Link, useLocation, useParams } from "react-router-dom";
import { toast } from "sonner";
import { FormError } from "../App/FormError";
import { ISpaceDialogProps } from "./ISpaceDialogProps";

export const CreateSpace: React.FC<ISpaceDialogProps> = ({
  addSpace,
  onClose,
  predefinedData,
  updateSpace,
}) => {
  const [file, setFile] = useState<File | null>(null);
  const [name, setName] = useState(predefinedData?.name || "");
  const [description, setDescription] = useState("");
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState({
    name: "",
    description: "",
    file: "",
  });
  const { facilityParams } = useParams<{ facilityParams: string }>();
  const location = useLocation();

  const id = facilityParams?.split("~")[0];
  const isUpdateView = !!predefinedData;

  useEffect(() => {
    if (predefinedData) {
      removeErrors();
      setName(predefinedData?.name || name);
      setDescription(predefinedData?.description || description);
      setOpen(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [predefinedData]);

  const removeErrors = () => {
    setErrors({ name: "", description: "", file: "" });
  };

  const resetForm = () => {
    setName("");
    setDescription("");
    setFile(null);
    setErrors({ name: "", description: "", file: "" });
  };

  const handleOpenChange = (isOpen: boolean) => {
    setOpen(isOpen);
    resetForm();
    if (onClose) {
      onClose();
    }
  };

  const formIsValid = () => {
    const nameError = validateName(name);
    const descriptionError = validateDescription(description);
    const fileError = validateFile(file, !predefinedData);
    setErrors({
      name: nameError,
      description: descriptionError,
      file: fileError,
    });
    return !nameError && !descriptionError && !fileError;
  };

  function handleName(event: React.FocusEvent<HTMLInputElement>) {
    const error = validateName(event.currentTarget.value);
    setErrors((prevErrors) => ({ ...prevErrors, name: error }));
  }

  function handleDescription(event: React.FocusEvent<HTMLTextAreaElement>) {
    const error = validateDescription(event.currentTarget.value);
    setErrors((prevErrors) => ({ ...prevErrors, description: error }));
  }

  const handleFileChange = (e: React.FocusEvent<HTMLInputElement>) => {
    if (e.target.files) {
      const fileError = validateFile(e.target.files[0], !predefinedData);
      setErrors((prevErrors) => ({ ...prevErrors, file: fileError }));
      setFile(e.target.files[0]);
    }
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    if (loading || !formIsValid()) {
      return;
    }

    try {
      setLoading(true);

      if (isUpdateView) {
        setOpen(false);

        const result = await update({
          id: predefinedData.id,
          file: file || undefined,
          description,
          name,
        });

        if (!result) {
          toast.error("Something went wrong while updating the space");
          return;
        }

        if (updateSpace) {
          updateSpace({
            id: predefinedData.id,
            description,
            name,
            blueprintUrl: result.blueprintUrl,
          });
        }

        toast.success("Successfully updated facility", {
          duration: 2000,
          action: (
            <Link
              to={`${location.pathname}/${predefinedData.id}`}
              className="text-blue-500"
            >
              View Layout
            </Link>
          ),
        });
      } else {
        setOpen(false);
        const newSpace: Omit<SpaceBody, "id"> = {
          name: name,
          description: description,
          file: file!,
          facilityId: id!,
        };
        const space = await create(newSpace);

        toast.success("Successfully created facility", {
          duration: 2000,
          action: (
            <Link
              to={`${location.pathname}/${space.id}`}
              className="text-blue-500"
            >
              View Layout
            </Link>
          ),
        });

        addSpace(space);
      }

      resetForm();
    } catch (e) {
      toast.error(getErrorMessage(e));
    } finally {
      setLoading(false);
    }
  };

  return (
    <Dialog open={open} onOpenChange={handleOpenChange}>
      <DialogTrigger asChild>
        <Button variant="ghost">
          <PlusIcon />
        </Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>
            {predefinedData ? "Update" : "Create"} Space
          </DialogTitle>
        </DialogHeader>

        <div>
          <Label htmlFor="name">Name</Label>
          <Input
            id="name"
            type="text"
            required
            onBlur={handleName}
            onChange={(e) => setName(e.currentTarget.value)}
            value={name}
          />
          <FormError error={errors.name} />
        </div>

        <div>
          <Label htmlFor="description">Description</Label>
          <Textarea
            id="description"
            className="min-h-[1rem]"
            required
            onBlur={handleDescription}
            onChange={(e) => setDescription(e.currentTarget.value)}
            value={description}
          />
          <FormError error={errors.description} />
        </div>

        <div>
          <Label htmlFor="blueprint">Blueprint</Label>
          <Input
            id="blueprint"
            type="file"
            className="file:text-secondary-foreground"
            onChange={handleFileChange}
          />
          <FormError error={errors.file} />
        </div>

        <DialogFooter>
          <Button type="submit" onClick={handleSubmit} className="text-white">
            Submit
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};
