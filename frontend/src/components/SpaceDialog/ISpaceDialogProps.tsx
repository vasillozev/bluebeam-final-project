import { SpaceResponse } from "@/services/space.service";

export type SpaceEditableFields = Pick<
  SpaceResponse,
  "description" | "name" | "blueprintUrl"
>;

export type SpaceModalPredefinedData = Partial<SpaceEditableFields> & {
  id: string;
};

export interface ISpaceDialogProps {
  addSpace: (space: SpaceResponse) => void;

  onClose?: () => void;
  updateSpace?: (updated: SpaceModalPredefinedData) => void;
  predefinedData?: SpaceModalPredefinedData;
}
