import { cn } from "@/lib/utils";
import { SearchIcon } from "lucide-react";
import { useEffect, useRef, useState } from "react";
import { useDebouncedCallback } from "use-debounce";
import { Input } from "./ui/input";

type MapboxFeature = {
  type: "Feature";
  id: string;
  geometry: {
    type: string;
    coordinates: [number, number];
  };
  properties: {
    mapbox_id: string;
    feature_type: string;
    full_address: string;
    name: string;
    name_preferred: string;
    coordinates: {
      longitude: number;
      latitude: number;
    };
    bbox: [number, number, number, number];
    context: {
      country: {
        mapbox_id: string;
        name: string;
        country_code: string;
        country_code_alpha_3: string;
        wikidata_id: string;
      };
    };
  };
};

export type Address = {
  coordinates?: {
    longitude: number;
    latitude: number;
  };
  full_address: string;
};

interface AddressInputProps {
  id?: string;
  withIcon?: boolean;
  placeholder?: string;
  // address: Address | null;
  initialQuery?: string;

  onSelect: (address: Address) => void;
}

export const AddressInput: React.FC<AddressInputProps> = ({
  id,
  withIcon,
  placeholder,
  initialQuery,
  onSelect,
}) => {
  const listRef = useRef<HTMLDivElement>(null);
  const [hidden, setHidden] = useState(true);
  const [suggestions, setSuggestions] = useState<Address[]>([]);
  const [query, setQuery] = useState(initialQuery || "");

  const getSuggestions = async (
    query: string,
    limit = 3,
  ): Promise<Address[]> => {
    const url = new URL("https://api.mapbox.com/search/geocode/v6/forward");

    url.searchParams.append("q", query);
    url.searchParams.append("limit", limit.toString());
    url.searchParams.append("access_token", import.meta.env.VITE_MAPBOX_KEY);

    const response = await fetch(
      url.toString() + "&proximity=23.324455%2C42.69194",
    );
    const { features } = (await response.json()) as {
      features: MapboxFeature[];
      attribution: string;
      type: "FeatureCollection";
    };

    return features.map((f) => ({
      id: f.id,
      coordinates: f.properties.coordinates,
      full_address: f.properties.full_address,
      name: f.properties.name,
      name_preferred: f.properties.name_preferred,
    }));
  };

  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (listRef.current && !listRef.current.contains(event.target as Node)) {
        setHidden(true);
      }
    };

    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  const debouncedGetSuggestions = useDebouncedCallback<(q: string) => void>(
    (q) =>
      getSuggestions(q).then((suggestions) => {
        setSuggestions(suggestions);
        setHidden(false);
      }),
    500,
  );

  return (
    <div>
      <div className="relative min-w-[240px]">
        {withIcon && (
          <SearchIcon className="absolute left-2 top-[50%] aspect-square h-4 translate-y-[-50%]" />
        )}

        <Input
          onChange={(e) => {
            const value = e.target.value;
            setQuery(value);
            debouncedGetSuggestions(value);
          }}
          onFocus={() => setHidden(false)}
          value={query}
          placeholder={placeholder}
          id={id || "address"}
          className={cn(
            "w-full outline-primary focus-visible:ring-0 focus-visible:ring-offset-0",
            withIcon && "pl-8",
          )}
        />
      </div>

      {!hidden && suggestions.length > 0 && (
        <div ref={listRef} className="relative">
          <ul
            className={`absolute left-0 top-0 mt-2 min-w-[240px] rounded-md border`}
          >
            {suggestions.map((s, idx) => (
              <li
                className="last:border-b-none z-10 flex w-full cursor-pointer flex-col gap-y-1 border-b bg-background p-2 first:rounded-t-md last:rounded-b-md hover:brightness-150"
                onClick={() => {
                  setSuggestions([]);
                  onSelect(s);
                  setQuery(s.full_address);
                }}
                key={idx}
              >
                <span className="text-sm font-semibold">{s.full_address}</span>
                <span className="text-xs text-muted-foreground">
                  {s.full_address}
                </span>
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
};
