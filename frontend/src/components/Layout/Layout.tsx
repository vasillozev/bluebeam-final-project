import { Toaster as Sonner } from "@/components/ui/sonner";
import React from "react";
import { AppNavBar } from "../App/NavBar";

interface ILayout {
  children: React.ReactNode;
}

function Layout({ children }: ILayout) {
  return (
    <>
      <AppNavBar />
      <main className="mx-auto mt-4 flex w-full flex-grow justify-center lg:w-11/12">
        {children}
      </main>
      <Sonner richColors />
    </>
  );
}

export default Layout;
