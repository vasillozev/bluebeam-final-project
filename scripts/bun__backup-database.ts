// !!! can only be ran with bun(https://bun.sh)

async function backup(name: string, url: string) {
  const res = await fetch(url);
  await Bun.write(
    `./backups/${name}.json`,
    JSON.stringify(await res.json(), null, 2)
  );
}

async function run() {
  backup("issues", "http://localhost:8081/api/v1/issue/all");
  backup("spaces", "http://localhost:8081/api/v1/space/all");
  backup("facilities", "http://localhost:8081/api/v1/facility/all");
}

run();
